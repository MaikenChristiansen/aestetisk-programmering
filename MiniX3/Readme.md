## MiniX3 - Throbber
![](BigBen.gif)


- [My program](https://maikenchristiansen.gitlab.io/aestetisk-programmering/MiniX3/)
- [My code](https://gitlab.com/MaikenChristiansen/aestetisk-programmering/-/blob/main/MiniX3/sketch.js)

This assignment of this week made me feel a bit divided because I had a lot of ideas but it was difficult for me to make the ideas in Atom. I sought help from classmates and instructors, but when we finally managed to make the program it contained a lot of syntaxes I did not quite understand. At the start of this course, I made a rule for myself, that if I do not understand code I can not use it in my program. I want to know what happens in my program and be able to explain it.

Therefore is my program for this week a plan C or even D. I have made a model of Big Ben in London. I have used some of the most fundamental syntaxes to build it, most of it is rects, ellipses, and beginShape/endShape, but there is a loop for the lines on the lower part of Big Ben and some rotate-syntaxes seen as the "arms" of the clock and the moon. I wanted the moon and the stars to symbolize a night cycle, but with the clock on Big Ben, it is not the night cycle we are used to. I have a bug in my program because it was hard for me to make the moon go behind the ceiling of Big Ben.
The clock on it should not be seen as a normal clock, as visible in the program the "arms" are spinning wild and not set up to seconds, minutes and hours-cycle or the day and night-time cycle.   

The clock on Big Ben is just spinning without any specific measurement with a connection to time. As Hans Lammerant mentions in his text "How Humans and machines negotiate the experience of time" is the experiences of cycles in life a part of measuring time. Instead of using the different cycles such as day/night and summer/winter, we started to use mechanical clocks which
> allowed for the unification of time lengths and thereby also standardized time.

So normally the mechanical clock is a symbol of the standardized time we have all over the world like 1 minute is 60 seconds and 1 hour is 60 minutes. In my program, the clock is not linked to any time measurements as those mentioned above, because it is just spinning randomly. The reason why I have made the clock spin randomly is that I thought about how funny it is that time can feel different. If you are having a good day with some friends it feels like the day is short, but if you have ever tried counting down to your birthday as a kid, you have probably felt that the time never ends, even though a day is 24 hours no matter what.

A theory presented already in 1877 by Paul Janet tries to answer why the speed of time seems different.
>At the age of one month, a week is a quarter of your whole life, so it's inevitable that it seems to last forever. At the age of 14, one year constitutes around 7% of your life, which seems to be a large amount of time too. But at the age of 30, a week is only a tiny percentage of your life, and at 50 a year is only 2% of your life, so your subjective sense is that these are insignificant periods of time which pass very quickly.

Paul Janet describes the different speeds of time depending on how old you are, if you are 50 the percent of the time a week lasts is only 2%. That could be the answer to why children feel like it takes forever before they have a birthday again and older people feel like it was only a couple of months since they had a birthday.

### What does a throbber communicate, and/or hide?

Before this course, I always saw throbbers as a symbol of an ongoing process, but as we talked about different throbbers at an instructor class, my thoughts changed. We were talking about the rainbow throbber that appears on Mac-machines when a program is not responding. I thought this throbber meant that the program was working on a process, but according to one of the first hits when making a google search on what this throbber means it says
>The spinning wheel of death appears as a sign that an application is trying to deal with more processes than it can handle at the moment.

So the little rainbow throbber does not mean that the program is working on the different tasks it has, but more like it is stuck with all the processes. The rainbow throbber may only appear for a few seconds, but sometimes is there nothing else to do other than restart the program.

We have become familiar with a lot of throbbers like the one that shows when a video on YouTube is loading, we know that we usually only have to wait a few seconds before the video starts. Sometimes the throbber has a little loading bar, which gets filled to show us how long the process is in its loading. Therefore when we see some throbbers we often think about time or progress, I think personally it is annoying when a throbber just spins like my own. When there is just a spinning throbber and nothing else, it is hard to see the progress in, what is mostly a loading process and the time seems to last forever. Especially if you are waiting for order confirmation on your food from the local pizza place. So even though that throbber does not have to represent the time it takes for an application to load, it has at least for me become a symbol of time.   

## References
- [Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018)](https://monoskop.org/log/?p=20190)

- [Why does time have different speed?](https://www.psychologytoday.com/us/blog/out-the-darkness/201107/why-does-time-seem-pass-different-speeds)

- [Mac spinning wheel of death](https://setapp.com/how-to/how-to-stop-the-spinning-color-wheel-on-mac)

- [Blinking stars](https://editor.p5js.org/ag3439/sketches/Skgh1ZQtQ)

- [Loops](https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch3_InfiniteLoops/sketch.js)
