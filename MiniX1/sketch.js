// I need this variable to change the height of my eyes when they blink
let eyeHeight = 150;
let eyeHeight1 = 80;
let eyeHeight2 = 20;

// This is the variable that I am going to use to count time
let counter = 0;

// I need something to track whether the eyes are closed or not so
// so I can open them when they are closed and close them when they are open
let eyesClosed = false;

function setup() {
  createCanvas(800,800);

}

function draw() {
  background(255,255,255);
  // Add deltaTime to counter each frame
  // Remember that deltaTime is the number of milliseconds
  // since the last frame
  counter = counter + deltaTime;

  // If eyesClosed is true && counter is greater than 200
  // that means we have been blinking for 200 milliseconds
  if (eyesClosed && counter > 200) {
    // Open the eyes by setting eyeHeight to be bigger
    // and setting the eyesClosed boolean to false
    eyeHeight = 150;
    eyeHeight1 =80;
    eyeHeight2 = 20;

    eyesClosed = false;
    // reset counter to 0 so that it can
    // start counting up to 1000 again
    counter = 0;
  // Otherwise if eyeClosed is false and counter > 2000
  } else if (counter > 2000) {
    // Close the eyes by setting eyeHeight to be small
    // and setting the eyeClosed boolean to true
    eyeHeight = 2;
    eyeHeight1 = 2;
    eyeHeight2 = 2;

    eyesClosed = true;
    // reset counter to 0 so that it can
    // start counting up to 1000 again
    counter = 0;
  }

//The head
  //The head is yellow because of the RGB color scale
  //The same amout of RED and GREEN makes the color red
  fill(255,255,0)
  //Stroke is the outline of the head
  stroke(0)
  //The first 2 numbers are where the ellipse is places i the coordinate system
  //The last 2 numbers are the height and the width of the ellipse
  ellipse(250,300,450,450)

//Left whites of eye
  //There isn't any outline of the big white circle in the left eye
  noStroke();
  //The color of the eye
  fill(255);
  //The first 2 numbers are where the ellipse is places i the coordinate system
  //The last 2 numbers are the height and the width of the ellipse
  circle(150,250,eyeHeight);

// Left iris
  let xc =constrain(mouseX, 125, 174);
  let xs = constrain(mouseY, 225,275);
  fill(0);
  circle(xc,xs,eyeHeight1);

// Left glare
  fill(255);
  circle(xc+20,xs-20,eyeHeight2);

//Right whites of eye
  //There isn't any outline of the big white circle in the right eye
  noStroke();
  //The color of the eye
  fill(255);
  //The first 2 numbers are where the ellipse is places i the coordinate system
  //The last 2 numbers are the height and the width of the ellipse
  circle(350,250,eyeHeight);

// Right iris
  let xc1 =constrain(mouseX, 325, 374);
  let xs1 = constrain(mouseY, 225,275);
  fill(0);
  circle(xc1,xs1,eyeHeight1);

// Right glare
  fill(255);
  circle(xc1+20,xs1-20,eyeHeight2);

//Mouth
  //The color of red
  fill(255,0,0);
  //The three ellipser that makes the mouth
  ellipse(225,400,95,60);
  ellipse(265,400,95,60);
  ellipse(245,405,135,60);

//The text on the canvas
  textSize(32);
  fill(0);
  text('Hello world!', 475, 100);
}
