## Give water to sunflower

![](Sunflower.gif)

- [My program](https://maikenchristiansen.gitlab.io/aestetisk-programmering/MiniX7/)
- [My code](https://gitlab.com/MaikenChristiansen/aestetisk-programmering/-/blob/main/MiniX7/sketch.js)

### Which MiniX have you reworked?
To figure out which MiniX I should revisit was an easy choice for me, due to Corona I did not have the surplus to do MiniX6. Therefore I ended up making the program we made in class with some small changes such as changing the figures. Most of the code was copied, due to that, I felt like I could do much better by creating my own code especially after seeing the codes and programs my classmates have made. See other peers' codes and programs inspired me to try to make my own code because I saw new opportunities due to fulfilling the assignment for the MiniX6.

### What have you changed and why?
So for this MiniX, I have created a whole new program. The program is about catching a waterdrop for a sunflower. The waterdrop is coming from the sky, where it is coded to appear on random coordinates of the X-axis between +20 and -20. The waterdrop is starting on the Y-coordinate -10, this makes it look like the waterdrop is staring outside the canvas. To catch the waterdrop is it possible to move the sunflower with the mouse. The X-coordinate of the mouse is set to the X-coordinate of the ellipses that together create the sunflower.

To make the score change the Y-coordinate of the waterdrop need to be -75, but at the same time the X-coordinate must be bigger than the X-coordinate of the mouse-20 and less than the X-coordinate of the mouse+20

This MiniX is about Object-Oriented Programming. "Object-Oriented Programming” (OOP), a paradigm of programming in which programs are organized around data, or objects, rather than functions and logic"

I have created the waterdrops as an object with data. The class of the waterdrop is some kind of a template, this template has the purpose to tell us what it means to be a waterdrop. In a way, you can say that the class is the things you would use to make a waterdrop. Inside the class, I use a function called a constructor which is the waterdrops setup, the constructer is all the data. The data the waterdrops has is the X and Y coordinates and the size and speed for it.

The waterdrops I use in my program can also be described by properties such as they are colored blue and do not have a stroke, and so on. Besides the properties, the waterdrops also have something called behavior. The behavior tells us what the object is doing, in this case, they are moving down on the Y-axis. To summarize you can say that the properties of an object are about how it looks like, and the behaviors are about what the object is doing, “all these hold and manage data so it can be used and operations can be performed”.

### What have you learnt in this mini X?
I chose to make a new program and therefore my main focus in this MiniX has been on the code and how to understand it. I really wanted to create something that did not look like the code we had in class, therefore I created a catching game, where you should catch water for a sunflower. I used a lot of the syntaxes we have worked with earlier in the course and I like how my program has ended up. I have been struggling a lot with some of the object-oriented programming aspects, but I think it is a lot better than the MiniX6. My ReadMe for MiniX6 was all about how to understand what object-oriented programming was and how to understand the different aspects of it, I tried to read the assigned readings but ended up watching some videos of Daniel Shiffman to understand it even better.
So in this MiniX I learnt how to use classes and constructors in a code, and understand what the different parts of the code mean.  


### What is the relation between aesthetic programming and digital culture? How does your work demonstrate the perspective of aesthetic programming (look at the Preface of the Aesthetic Programming book)?
So aesthetic programming you can express your feelings, thoughts, and opinions about certain topics. I really liked the MiniX2 about emojis, because I felt the issue about people didn't feel represented in the 3,633 emojis (On IOS - September 2021) was an anthropogenic problem. I have the opinions that I do not need to feel represented by my emojis keyboard, because I use them as a symbol for my feelings about a certain text and not as something that should symbolize me as a human. Therefore, I  almost did in protest two emojis that did not look like humans because I just wanted people to use them as a symbol of their mood or feelings.

But in a way, I understand people who feel not represented in the emojis because of their sexuality, but I also think the emojis were two women or men kisses (👩‍❤️‍💋‍👩  👨‍❤️‍💋‍👨  ) is a part of the culture we live in right now. Because back in the days it was not very popular to be homosexual and that could give us an answer about why these emojis were not included in the first releases of emojis. At the same time, the use of emojis has tended to grow and grow in the past years, and it seems to have changed our use of emojis too. We started to use them as a funny thing in our text messages, but their use today seems to have changed so that we now use them as symbols of us-self as humans.

In this way, aesthetic programming gives us the opportunities to express ourselves and at the same time makes us reflect on how the world has changed especially when it comes to digital concepts and culture.

## References
[Aesthetic programming - W. Soon & G. Cox](https://aesthetic-programming.net/pages/preface.html)

[Daniel Shiffman - 6.2 Classes in JavaScript with ES6 - p5.js Tutorial](https://www.youtube.com/watch?v=T-HGdc8L-7w)

[What new emojis are available with Apple’s iOS 15?](https://www.hitc.com/en-gb/2021/09/21/ios-15-new-emojis/)
