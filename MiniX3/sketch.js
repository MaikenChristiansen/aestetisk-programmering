//The variables of the "arms" of the clock and the variable of the moon
let time=0
let time2=1
let motion=2

function setup() {
 //creating the canvas for the program
 createCanvas(windowWidth, windowHeight);
}

function draw (){
//background with transparancy
  background(0,0,35,25);

//blinking stars
  let stars = {
//The X and Y location is random but in between the width and hight of the canvas
  locationX : random(width),
  locationY : random(height),
//The size of the start variates between 1 and 10
  size : random(1,10)
 }
/*The stars shows up and different X and Y locations in between the height and width of the canvas
at the same time they have different sizes between 1 to 10*/
  ellipse(stars.locationX ,stars.locationY, stars.size, stars.size);


//The main building
push();
  fill(239,219,112);
  rect(540,425,370,250);

  fill(228,183,61);
  rect(600,675,250,800);
  rect(600,425,250,250);
  rect(570,380,310,45);
  rect(600,310,250,30);

  fill(79,95,109);
    beginShape();
    vertex(570,380);
    vertex(880,380);
    vertex(850,335);
    vertex(600,335);
    endShape();
    triangle(600,310,850,310,720,20)
pop();


//The clock on Big Ben
push();
  ellipseMode(RADIUS);
  fill(255);
// Outer white ellipse
  ellipse(725, 550, 120, 120);
//How to get the inner ellipse to be in the center of the outer ellipse
  ellipseMode(CENTER);
// Inner white ellipse
  ellipse(725, 550, 180, 180);
pop();

//The small rects of the clock
push();
fill(79,95,109);
  rect(720,435,10,20);
  rect(720,645,10,20);
  rect(610,545,20,10);
  rect(820,545,20,10);
pop();

//The loop of the lines on Big Ben
  let x=575
  let x1=300
  for(let i=0;i<5;i++){
    x+=50;
    let y=700
    for(let i2=0;i2<4;i2++){

push();
  fill(255,255,0);
  strokeWeight(2);
  line(x,y,x,675);
  y+=70;
pop();

}}

//The "arms" of the clock
angleMode(DEGREES);
//The point we want the "arms" to rotate around
translate(720, 550);
push();
  rotate(time)
  stroke(0);
  strokeWeight(5)
  line(0,0,50,10);
  time++;
pop();

push();
  rotate(time2)
  stroke(255,0,0);
  strokeWeight(5)
  line(0, 0, 70, 10);
  time2+=5
pop();

//The moon
//The point we want the "arms" to rotate around - Above we made 0,0 = 720,550.
translate(0,450);
push();
rotate(motion);
  ellipse(750,50,70);
motion+=0.5;
pop();

}
 function windowResized() {
   resizeCanvas(windowWidth, windowHeight);
 }
