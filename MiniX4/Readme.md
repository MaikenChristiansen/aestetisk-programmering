![](WhileTyping.gif)

- [Run My Program](https://maikenchristiansen.gitlab.io/aestetisk-programmering/MiniX4/)
- [My Code](https://gitlab.com/MaikenChristiansen/aestetisk-programmering/-/blob/main/MiniX4/sketch.js)

## While typing

I have created a program I have want to call "While typing".
"While typing" is a program which makes it possible for you to create your own personalized artwork while you are typing the keys on the keyboard of your laptop. I got the inspiration for this program, while reading and learning about the NFTs in a course called Software Studies. NFTs is abbreviation for non-fungible token which in this case would mean that the personalized artwork is unique and can not be replaced with something else. NFTs can be anything digital for example a drawing or some music. Today NFTs can work as an asset it means that you can buy some digital art and hope that the value of it goes up, so you can sell it for a profit, just like the bank shares most of us know today.

So how does the artwork "While typing" work like a NFT? When you start to type the different keys on your keyboard (Only in lower case letters, the spacebar and Enter). You start to create your own personalized artwork. Each keys is connected to different shapes from the shape libray in P5.js., some of the keys is connected to the same shapes. That means when you type a key on the keyboard a shape appears on the screen in that way you are creating your own personalized unique artwork.
After writing your name or what ever you want to write you press Enter to symbolize that you are done with your typing. The you can open the console of the computer where the keys you have typed is shown.

#### The program
In this program I really tried to think of capture of data in other ways than using the webcam or microphone of a laptop. I think everyone have heard the stories about someone whos webcam was hacked and recording them without them being aware of it. Or tried that after talking about something specific getting ads about it.

Therefore have I created a program which takes the data of the keys typed on a keyboard. In the program is used different syntaxes for the keys and mouse. At the introductionScreen of the program is the syntax for mousePressed used. The function is used to switch between the introductionScreen to the drawingScreen. When you are on the drawingScreen the syntax keyTyped is used to make the different shapes on the screen. All the letters is pushed into the open array (bogstaver) written in the start of the code with the function bogstaver.push.
Then the keyPressed is used when typing the Enter-key, when typing enter after writing and creating art, the console is cleared so the only thing that stand in the console is the end-result of the typing.


#### Why should data like keys typed be captured?
The data of keys could be used just like the data that can be captured microphone. You have maybe tried it yourself that when you have talked to someone about a specific pair of shoes, then boom there is an add that shows them on your phones.
Just like this firms could capture the data of the keys there is typed and personalize your adds. At the same time firms could used the data to track and redirect consumers to their website.  

It can also be used to create unique artworks such as logos and stories. If you for example try to the type a passage of the Bible, it makes this passage artwork unique. This can be sold as an NFT and be an asset for the creater. It is impossible for other people to re-create the passage, which makes it unique.


### References
- [DOM syntaxes](https://p5js.org/reference/#group-DOM)
- [Make scenes change](https://www.youtube.com/watch?v=RlsRQS5qFSY&fbclid=IwAR3quJBVDJBkyqAhBUHrUmC9h0AvsnFUP6iPNK8rVQdgdFgjE_FjRFlIoPI)
- [Aesthetic programming - W. Soon & G. Cox](https://aesthetic-programming.net/pages/4-data-capture.html#minix-capture-all-15243)
