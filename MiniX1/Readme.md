## MiniX1 - Smiley with eyes

![](MiniX1_Smiley_with_eyes.png)


**What have you produced?**
For this week MiniX I have created a smiley. The smiley have a set of eyes which moves when you move around with the mouse. With help from the deltaTime function, which is a form of timer, is the eyes blinking in every 2000 milliseconds.

**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**
To be honest I think it was very hard and I had a lot of troubles to make the eyes blink. I tried out several of codes, but nothing seemed to work the way I would like it to work, but suddenly it worked in a way that I could accept. I had some kind of breakdowns, where I just wanted to throw my computer in the trash can. But with help from the instructors and classmates I managed to make some kind of code, where I think you can se the smiley and the little blink in the eyes.
I think it can be a bit difficult to figure out how the codes in the references works, especially when you want to adjust them into your own code. Hopefully it will be easier further in this course.

**How is the coding process different from, or similar to, reading and writing text?**
I find the coding process similar especially to reading an instruction manual. You have to read the code in a specific order to understand what there is happening. You can not really skip the easy way, you have to follow the steps of the codes.
It is different from writing a text because you have to put every single sign correct before the code work, I think you can combine the mistakes in a code with grammar errors. It can be hard to find your mistakes because there is not some kind of spell checker like there is in programs such as Word and so on.
But coding seems to me to be another language. In Anette Vees
>Coding for Everyone and the Legacy of Mass Literacy pp. 43-93

Coding is compared to literacy because it is some kind of language, but it is not something we learn our students even though it has a bigger impact on us now than ever before. I actually think if there was a
>(...) two-hour-per-week semester course in coding, (...)

as George E. Forsythe recommended in 1959, a lot of people would be encouraged to code and learn how to code. I do not think it is impossible to learn how to code, but I know i would not have learnt it if I have not had this course. It seems a bit overwhelming to try to learn it, especially when the codes for some reason does not work. Therefor I think every human should have the opportunity to learn this new language, so we can talk this new language together.

**What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?**
Code and programming has not had a huge impact on me, because I struggle with recognize its type of language and find the system in it right now. I try to think about it as an instruction manual. If you are lost in an instruction manual, you will go a step back and then look carefully at it and try again. But if you are lost in a code, you are completely lost.
You need to know how to read and write code before it make sense to you, and maybe that is the thing that scares a lot of people to program.


## Link to project
- [Execute my project](https://maikenchristiansen.gitlab.io/aestetisk-programmering/MiniX1/)
- [See my projects code](https://gitlab.com/MaikenChristiansen/aestetisk-programmering/-/blob/main/MiniX1/sketch.js)

##### References
- [To make the eyes follow the cursor](https://editor.p5js.org/edwardjmartin/sketches/XbjbWuuKG?fbclid=IwAR1L5roljvAVGokzOipNBO6ccKJM5KzM2-rDjiMWtCB3SLV9aDoV9K2EbSE)
- [To make the eyes blink](https://editor.p5js.org/stalgiag/sketches/mzpdFPhdx)
