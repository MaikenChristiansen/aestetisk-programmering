let screen = 0;
let score= 0;
let drop;

function setup() {
  createCanvas(windowWidth, windowHeight);
  drop = new Raindrop();
}

function draw() {
//If screen is equal to 0 show the startScreen
  if(screen == 0){
    startScreen();
  }
  //If screen is equal to 1 show the gameScreen
  else if(screen == 1){
  	gameScreen();
  }
  //If screen is equal to 2 show the endScreen
  else if(screen==2){
  	endScreen();
  }
}

//Designing the startScreen
function startScreen(){
  background(227,194,184);
  fill(255);
  textAlign(CENTER);
  textSize(40);
  text('GIVE WATER TO THE SUNFLOWER!', width / 2, height / 2);
  textSize(20);
  text('click anywhere to start', width / 2, height / 2 + 40);
	reset();
}
//Designing the gameSreen
function gameScreen(){
  background(227,194,184);
  text("score = " + score, 50,25);
  noStroke();

  //clouds
	fill("white");
  //cloud 1
	ellipse(200,30,150,50);
	ellipse(150,50,150,50);
	ellipse(250,50,150,50);
  //cloud 2
  ellipse(600,30,150,50);
	ellipse(550,50,150,50);
	ellipse(650,50,150,50);
  ellipse(700,40,150,50);
  //cloud 3
  ellipse(1000,30,150,50);
	ellipse(950,50,150,50);
	ellipse(1050,50,150,50);
  ellipse(1100,40,150,50);



  //sunflower
  fill("Yellow");
  ellipseMode(CENTER);
  ellipse(mouseX+25,height-50,50,50);
  ellipse(mouseX-25,height-50,50,50);
  ellipse(mouseX,height-75,50,50);
  ellipse(mouseX,height-25,50,50);
  fill("Brown");
  ellipse(mouseX,height-50,40,40);

  drop.move();
  drop.show();

  if(drop.y>height){ //If the Y-coordinate of the drop > than the height of the canvas show screen 2, which is endScreen
  	screen =2;
	 }

  fill(255);
  //If the Y-coordinate of the drop is -75 and the X-coordinate of the drop is > the X-coordinate of the mouse-20 and at the same time < than the X-coordinate of the mouse+20
  //Plus 1 to the score
  if(drop.y>height-75 && drop.x>mouseX-20 && drop.x<mouseX+20){
    score+= 1;
//If the drop is catched create a new drop
    drop.reset();
  }
}

function endScreen(){
  background(168,55,45);
  textAlign(CENTER);
  fill(255);
  textSize(40);
  text('GAME OVER!', width / 2, height / 2)
  textSize(20);
  text("You missed a waterdrop and sunflower is dried out", width / 2, height / 2 + 40);
  text("SCORE = " + score, width / 2, height / 2 + 60);
	text('click anywhere to play again', width / 2, height / 2 + 80);
}

function mousePressed(){
//If the startScreen is shown and the mouse is clicked switch to screen 1/gamescreen
  if(screen==0){
  	screen=1;
  }
//If the endScreen is shown and the mouse is clicked switch to screen 0/startScreen
  else if(screen==2){
  	screen=0;
  }
}

function reset(){
  score=0;
  drop.speed=2;
  drop.y=-10;
}

class Raindrop{

  constructor(){
    this.y = -10 //The Y-coordinate where the drops starts
    this.x = random(20,width-20) //Where the drops is placed on the X-coordinates
    this.size = 25 //The size of the drop
    this.speed = 2 //The speed of the waterdrop
  }

  move(){
    //The raindrop is moving on Y-axis in the declared speed
    this.y+= this.speed

  }

  show(){
    fill(7,179,223);
    //The raindrop
    ellipse(this.x,this.y,this.size,this.size);
  }

  reset(){
    this.y = -10 //The Y-coordinate where the drops starts
    this.x = random(20,width-20) //Where the drops is placed on the X-coordinates
    this.speed+= 0.5 //When one drop is catched the speed of the next drop increases with 0.5
  }
}
